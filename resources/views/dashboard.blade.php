@extends('layouts.app')

@section('content')
    @include('layouts.headers.cards')

    <div class="container-fluid mt--7">
        <div class="row">
            <div class="col-xl-12 mb-5 mb-xl-0 h-100vh">
                @include('maps.mapShow')
            </div>
        </div>

        @include('layouts.footers.auth')
    </div>
@endsection

@push('js')
    <script type="text/javascript">
        $(document).ready(function () {
            var map = new google.maps.Map(document.getElementById('map'), {
                zoom: 10,
                center: new google.maps.LatLng(26.074524545055738, 50.5481447212037),
                mapTypeId: google.maps.MapTypeId.ROADMAP
            });
            var infowindow = new google.maps.InfoWindow();
            refreshMap();
            function refreshMap() {
                console.log('start');
                $.ajax({
                    headers: {
                        'X-CSRF-Token': $('meta[name="_token"]').attr('content')
                    },
                    url:'http://corona.alifouad91.com/users/',
                    type:'GET',//GET POST PUT DELETE
                    dataType:'JSON',//Type of Data to Send And Receive,
                    success:function (response) {
                        var users = response.data;
                        var marker;
                        for ( var i = 0;i<users.length;i++) {
                            if (users[i].status === 'QI') url="http://maps.google.com/mapfiles/ms/icons/red-dot.png";
                            if (users[i].status === 'NQ') url="http://maps.google.com/mapfiles/ms/icons/green-dot.png";
                            if (users[i].status === 'QNI') url="http://maps.google.com/mapfiles/ms/icons/yellow-dot.png";

                            marker = new google.maps.Marker({
                                position: new google.maps.LatLng(users[i].lat, users[i].long),
                                map: map,
                                icon: {
                                    url: url
                                }
                            });
                            google.maps.event.addListener(marker, 'click', (function(marker, i) {
                                return function() {
                                    infowindow.setContent(users[i].name +'  ' +users[i].type );
                                    infowindow.open(map, marker);
                                }
                            })(marker, i));
                        }
                        $('#nq').text(response.nq);
                        $('#qi').text(response.qi);
                        $('#qni').text(response.qni);
                        //last_qi
                        var today = new Date();
                        var date = today.getFullYear()+'-'+(today.getMonth()+1)+'-'+today.getDate();
                        var time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
                        var dateTime = date+' '+time;
                        $('#last_nq').text('updated at : '+ dateTime);
                        $('#last_qi').text( 'updated at : '+ dateTime);
                        $('#last_qni').text('updated at : '+ dateTime);
                    }
                });
            }
            setInterval(refreshMap,5000)
        });
    </script>
    <script src="{{ asset('argon') }}/vendor/chart.js/dist/Chart.min.js"></script>
    <script src="{{ asset('argon') }}/vendor/chart.js/dist/Chart.extension.js"></script>
@endpush
