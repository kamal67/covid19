<div class="header bg-gradient-primary pb-8 pt-5 pt-md-8">
    <div class="container-fluid">
        <div class="header-body">
            <!-- Card stats -->
            <div class="row">
                <div class="col-xl-4 col-lg-4 col-12">
                    <div class="card card-stats mb-4 mb-xl-0">
                        <div class="card-body">
                            <div class="row">
                                <div class="col">
                                    <h5 class="card-title text-uppercase text-muted mb-0">Quarantined and Infected:</h5>
                                    <span class="h2 font-weight-bold mb-0" id="qi">{{
                                      count($user_qi)
                                    }}</span>
                                    <br>
                                    <small class="   mb-0" id="last_qi">{{ now('Asia/Bahrain')->format('Y-m-d h:i:s') }}</small>
                                </div>
                                <div class="col-auto">
                                    <div class="icon icon-shape bg-danger text-white rounded-circle shadow">
                                        <i class="fas fa-user-alt-slash"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-4 col-lg-4 col-12">
                    <div class="card card-stats mb-4 mb-xl-0">
                        <div class="card-body">
                            <div class="row">
                                <div class="col">
                                    <h5 class="card-title text-uppercase text-muted mb-0">Quarantined no infection</h5>
                                    <span class="h2 font-weight-bold mb-0" id="qni">{{count($user_qni)}}</span>
                                    <br>
                                    <small class="   mb-0" id="last_qni">{{ now('Asia/Bahrain')->format('Y-m-d h:i:s') }}</small>
                                </div>
                                <div class="col-auto">
                                    <div class="icon icon-shape bg-yellow text-white rounded-circle shadow">
                                        <i class="fas fa-users"></i>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
                <div class="col-xl-4 col-lg-4 col-12">
                    <div class="card card-stats mb-4 mb-xl-0">
                        <div class="card-body">
                            <div class="row">
                                <div class="col">
                                    <h5 class="card-title text-uppercase text-muted mb-0">Not quarantined:</h5>
                                    <span class="h2 font-weight-bold mb-0" id="nq">{{count($user_nq)}}</span>
                                    <br>
                                    <small class="   mb-0 " id="last_nq">{{ now('Asia/Bahrain')->format('Y-m-d h:i:s') }}</small>
                                </div>
                                <div class="col-auto">
                                    <div class="icon icon-shape bg-green text-white rounded-circle shadow">
                                        <i class="fas fa-users"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {{--<div class="col-xl-3 col-lg-6">--}}
                {{--<div class="card card-stats mb-4 mb-xl-0">--}}
                {{--<div class="card-body">--}}
                {{--<div class="row">--}}
                {{--<div class="col">--}}
                {{--<h5 class="card-title text-uppercase text-muted mb-0">Performance</h5>--}}
                {{--<span class="h2 font-weight-bold mb-0">49,65%</span>--}}
                {{--</div>--}}
                {{--<div class="col-auto">--}}
                {{--<div class="icon icon-shape bg-info text-white rounded-circle shadow">--}}
                {{--<i class="fas fa-percent"></i>--}}
                {{--</div>--}}
                {{--</div>--}}
                {{--</div>--}}
                {{--<p class="mt-3 mb-0 text-muted text-sm">--}}
                {{--<span class="text-success mr-2"><i class="fas fa-arrow-up"></i> 12%</span>--}}
                {{--<span class="text-nowrap">Since last month</span>--}}
                {{--</p>--}}
                {{--</div>--}}
                {{--</div>--}}
                {{--</div>--}}
            </div>
        </div>
    </div>
</div>
