<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
/*Auth Group */
Route::group(['prefix' => 'auth'], function () {
    Route::post('/login/', [\App\Http\Controllers\Api\AuthController\AuthController::class, 'login']);
    Route::post('/register/visitor', [\App\Http\Controllers\Api\AuthController\AuthController::class, 'registerVisitor']);
    Route::post('/register/citizen', [\App\Http\Controllers\Api\AuthController\AuthController::class, 'registerCitizen']);
});
Route::group(['middleware'=>'jwt.auth','prefix'=>'user'],function (){
    Route::get('/info/', [\App\Http\Controllers\Api\AuthController\AuthController::class, 'info']);
    Route::post('/update/status/', [\App\Http\Controllers\Api\User\UserController::class, 'updateStatus']);
    Route::post('/update/location/', [\App\Http\Controllers\Api\User\UserController::class, 'updateLocation']);
    Route::get('/last-location/',[\App\Http\Controllers\Api\User\UserController::class,'getLastLocation']);
    Route::get('/all/my-places/',[\App\Http\Controllers\Api\User\UserController::class,'getAllUserPlaces']);
    Route::post('/update/image',[\App\Http\Controllers\Api\User\UserController::class,'updateImageProfile']);
    Route::get('/all/{status}',[\App\Http\Controllers\Api\User\UserController::class,'getAllUserByStatus']);
});

