<?php

namespace App\Models\UserPlaces;

use App\User;
use Illuminate\Database\Eloquent\Model;

class UserPlaces extends Model
{
    protected $fillable = [
        'user_id',
        'lat',
        'long',
    ];
    public function user()
    {
        return $this->hasOne(User::class,'user_id','id');
    }
}
