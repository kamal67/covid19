<?php

namespace App\Http\Controllers\Api\AuthController;

use App\Http\Controllers\Controller;
use App\Http\Controllers\JWTController;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Namshi\JOSE\JWT;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Facades\JWTAuth;


class AuthController extends JWTController
{
    public function login(Request $request)
    {
        $rules = [
            'phone' => 'required|string|max:191',
            'password' => 'required',
        ];

        $credentials = $request->only('phone', 'password');
        $validator = Validator::make($credentials, $rules);
        if ($validator->fails()) {
            return response()->json(['status' => false, 'Messages' => $validator->getMessageBag()], 400);
        }
        try {
            if ($token = JWTAuth::attempt(['phone' => $request->phone, 'password' => $request->password])) {
                return response()->json(['status' => true, 'token' => $token], 200);
            } else {
                return response()->json(['status' => false, 'Messages' => 'Invalid Data '], 401);
            }
        } catch (\JWTException $ex) {
            return response()->json(['status' => false, 'Messages' => $ex->getMessage()], 500);
        }
    }

    public function registerCitizen(Request $request)
    {
        $rules = [
            'password' => 'required',
            'type' => 'required|string|max:191',
            'phone' => 'required|string|max:30|unique:users',
            'email' => 'required|unique:users',
            'lat' => 'required',
            'long' => 'required',
            'name'=>'required'
        ];
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return response()->json(['status' => false, 'Messages' => $validator->getMessageBag()], 400);
        }
        try {
            if ($request->hasFile('image')) {
                $path = '/images/users/' . time() . $request->file('image')->getClientOriginalName();
                $request->file('image')->move('images/users/', $path);
            }
            if (empty($path)) {
                $path = '/images/default/default.png';
            }
            if ($request->type == 'citizen') {
                $user = User::query()->create(['name'=>$request->name,'lat' => $request->lat, 'long' => $request->long,'type' => $request->type, 'password' => Hash::make($request->password), 'status' => $request->status, 'phone' => $request->phone, 'email' => $request->email, 'image' => $path]);
                $token = JWTAuth::getFacadeRoot()->fromUser($user);
                return response()->json(['status' => true, 'token' => $token], 201);
            }
            return response()->json(['status' => false, 'message' => 'data in correct'], 400);
        } catch (\Illuminate\Database\QueryException $ex) {
            return response()->json(['status' => false, 'Messages' => $ex->getMessage()], 500);
        }
    }

    public function registerVisitor(Request $request)
    {
        $rules = [
            'password' => 'required',
            'passport' => 'required|unique:users',
            'type' => 'required|string|max:191',
            'phone' => 'required|string|max:30|unique:users',
            'lat' => 'required',
            'long' => 'required',
            'name'=>'required'
        ];
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return response()->json(['status' => false, 'Messages' => $validator->getMessageBag()], 400);
        }
        try {
            if ($request->hasFile('image')) {
                $path = '/images/users/' . time() . $request->file('image')->getClientOriginalName();
                $request->file('image')->move('images/users/', $path);
            }
            if (empty($path)) {
                $path = '/images/default/default.png';
            }
            if ($request->type == 'visitor') {
                $request->validate(['passport' => 'required|unique:users']);
                $user = User::query()->create(['name'=>$request->name,'lat' => $request->lat, 'long' => $request->long, 'type' => $request->type, 'password' => Hash::make($request->password), 'status' => $request->status, 'phone' => $request->phone, 'passport' => $request->passport, 'email' => null, 'image' => $path]);
                $token = JWTAuth::getFacadeRoot()->fromUser($user);
                return response()->json(['status' => true, 'token' => $token], 201);
            }
            return response()->json(['status' => false, 'message' => 'data in correct'], 400);
        } catch (\Illuminate\Database\QueryException $ex) {
            return response()->json(['status' => false, 'Messages' => $ex->getMessage()], 500);
        }
    }

    public function info()
    {
        try {
            $user = $this->getAuthenticatedUser();
            return response(['status' => true, 'data' => $user], 200);
        } catch (\Exception $ex) {
            return response()->json(['status' => false, 'Messages' => $ex->getMessage()], 500);
        }
    }
}
