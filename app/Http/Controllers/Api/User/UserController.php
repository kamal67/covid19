<?php

namespace App\Http\Controllers\Api\User;

use App\Http\Controllers\Controller;
use App\Http\Controllers\JWTController;
use App\Models\UserPlaces\UserPlaces;
use App\User;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class UserController extends JWTController
{
    public function updateStatus(Request $request)
    {
        $code = 200;
        $rules = [
            'status'=>'required'
        ];
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            $code =400;
            return response()->json(['status' => false, 'Messages' => $validator->getMessageBag()], $code);
        }
        try {
            $user_session = $this->getAuthenticatedUser();
            $user_session->status = $request->status;
            $user_session->save();
            return response()->json(['status' => true, 'data' => $user_session], $code);
        } catch (\Exception $ex) {
            $code = 500;
            return response()->json(['status' => false, 'Messages' => $ex->getMessage()], $code);

        }
    }

    public function updateLocation(Request $request)
    {
        $code = 200;
        $rules = [
            'lat'=>'required',
            'long'=>'required'
        ];
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            $code =400;
            return response()->json(['status' => false, 'Messages' => $validator->getMessageBag()], $code);
        }
        try {
            $user_session = $this->getAuthenticatedUser();
            $user_session->lat = $request->lat;
            $user_session->long = $request->long;
            $user_session->save();
            UserPlaces::query()->create(['user_id'=>$user_session->id,'lat'=>$request->lat,'long'=>$request->long]);
            return response()->json(['status' => true, 'data' => $user_session], $code);
        } catch (\Exception $ex) {
            $code = 500;
            return response()->json(['status' => false, 'Messages' => $ex->getMessage()], $code);

        }
    }

    public function getLastLocation()
    {
        try {
            $user_session = $this->getAuthenticatedUser();
            $data = ['lat'=>$user_session->lat,'long'=>$user_session->long];
            return response()->json(['status' => true, 'data' => $data], 200);
        } catch (QueryException $ex) {
            return response()->json(['status' => false, 'Messages' => $ex->getMessage()], 500);
        }
    }

    public function getAllUserPlaces()
    {
        try {
            $user_session = $this->getAuthenticatedUser();
            $data = UserPlaces::query()->where('user_id',$user_session->id)->orderBy('created_at','desc')
            ->select('lat','long','created_at')->get();
            return response()->json(['status' => true, 'data' => $data], 200);
        } catch (QueryException $ex) {
            return response()->json(['status' => false, 'Messages' => $ex->getMessage()], 500);
        }
    }

    public function updateImageProfile(Request $request)
    {
        $rules = [
            'image' => 'required',
        ];

        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return response()->json(['status' => false, 'Messages' => $validator->getMessageBag()], 400);
        }
        try {
            if ($request->hasFile('image')) {
                $user = $this->getAuthenticatedUser();
//                unlink(public_path() . $user->image);
                $path = '/images/users/' . time() . $user->id . $request->file('image')->getClientOriginalName();
                $user->image = $path;
                $user->save();
                $request->file('image')->move('images/users/', $path);
                return response()->json(['status' => true, 'data' => $user], 200);
            } else {
                return response()->json(['status' => false, 'Message' => "User Not Found"], 404);
            }
        } catch (\Exception $ex) {
            return response()->json(['status' => false, 'Messages' => $ex->getMessage()], 500);
        }
    }

    public function getAllUserByStatus($status)
    {
        try {
        $data = User::query()->where('status',$status)->select('name','type','lat','long','email','passport')->get();
        return response()->json(['status'=>true,'data'=>$data]);
        } catch (\Exception $ex) {
            return response()->json(['status' => false, 'Messages' => $ex->getMessage()], 500);
        }
    }
}
