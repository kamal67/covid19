<?php

namespace App\Http\Controllers;

use App\Models\Admin\Admin;
use App\User;
use App\Http\Requests\UserRequest;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    /**
     * Display a listing of the users
     *
     * @param  \App\User  $model
     * @return \Illuminate\View\View
     */
    public function index(User $model)
    {
        $users = User::all();
        $user_qni = User::query()->where('status','=','QNI')->get();
        $user_qi = User::query()->where('status','=','QI')->get();
        $user_nq = User::query()->where('status','=','NQ')->get();
        return view('users.index',compact('users','user_nq','user_qi','user_qni'));
    }
}
