<?php

namespace App\Http\Controllers;

use App\User;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $user_qni = User::query()->where('status','=','QNI')->get();
        $user_qi = User::query()->where('status','=','QI')->get();
        $user_nq = User::query()->where('status','=','NQ')->get();
        return view('dashboard',compact('user_nq','user_qi','user_qni'));
    }
}
